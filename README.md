
## CS185C - Social Network Analysis 
Team Members: (Inhee | Vrinda | Anirudh)
---------

Project: Power of BTS ARMY for Social Change Envisaged by Twitter Network Analysis
---------
<blockquote>
In June 2020, BTS (South Korean hip-hop boy group) donated one million dollars 
for Black Lives Matter (BLM). The BTS official fanclub, called ARMY, 
had mobilised via Twitter and decided to match the donation. They used Twitter 
hashtags #MatchAMillion, #Match1Million and #MatchTheMillion to spread word about 
the campaign. Within 24 hours between 7th-8th of June, 2020, the fundraising 
account @OneInAnARMY announced that they had met their goal of 1 million dollars 
for BLM. 

BTS Army is known for supporting various causes, so the initial campaign was 
not surprising. What was surprising was the speed at which this feat had been 
completed. This 24 hour window became the focal point of our analysis.
</blockquote>

### [Report](https://gitlab.com/ipark/cs185c-sna/-/tree/master/SNAproject/FinalReport/CS185C_FinalReport_Vrinda_Inhee_Anirudh.pdf) | [Slide](https://gitlab.com/ipark/cs185c-sna/-/blob/master/SNAproject/Presenation/CS185C-BLMandBTS.pdf)

Key Words
---------
BTS, ARMY, BLM, Social Network, Twitter, Louvain, Dynamic Network, Semantic Network, Power Law, 
Local Bridges, #MatchAMillion 

<img src="img/BLM.png" width="550">
<img src="img/BTS.png" width="550">
<img src="img/data-prep.png" width="550">
<img src="img/DN.png" width="700">
<img src="img/LB.png" width="550">
<img src="img/LB2.png" width="550">
<img src="img/SA.png" width="550">
